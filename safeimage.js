/**
* @fileOverview Implements a secure image system for websites
* @author <a href="mailto:bmease@gmail.com">Brad Mease</a>
* @version 0.0.1
*/

// List of html element attributes
var ATTRIBUTES = ['class','id','itemid','itemprop','itemref','itemscope','itemtype','lang','spellcheck',/*'style',*//*'tabindex',*/'title'];
var EVENTS = ['onabort','onblur','oncanplay','oncanplaythrough','onchange','onclick','oncontextmenu','oncuechange','ondblclick','ondrag','ondragend','ondragenter','ondragleave','ondragover','ondragstart','ondrop','ondurationchange','onemptied','onended','onerror','onfocus','onformchange','onforminput','oninput','oninvalid','onkeydown','onkeypress','onkeyup','onload','onloadeddata','onloadedmetadata','onloadstart','onmousedown','onmousemove','onmouseout','onmouseover','onmouseup','onmousewheel','onpause','onplay','onplaying','onprogress','onratechange','onreadystatechange','onscroll','onseeked','onseeking','onselect','onshow','onstalled','onsubmit','onsuspend','ontimeupdate','onvolumechange','onwaiting'];

function Log(msg, level) {
  var debugMsgs = ['[Critical]', '[Error]', '[Warning]', '[Info]', '[Debug]']
  var supportsConsole = (console.log) ? true : false;
  
  if (supportsConsole) {
    console.log(debugMsgs[level], msg);
  }
}

function Debug(msg) { Log(msg, 4); }
function Info(msg) { Log(msg, 3); }
function Warn(msg) { Log(msg, 2); }
function Error(msg) { Log(msg, 1); }
function Critical(msg) { Log(msg, 0); }

/**
*  Error
*/
function InvalidSimgParameters(msg) {
  var name = 'InvalidSimgParameters';
  this.msg = msg;

  return {name: name, msg: this.msg};
}


/**
* @class Represents a safe image
* @param {HTMLObject} pointer to the simg
* @param {String} image Image that should be obscured
* @throws {InvalidSimgParameters} if no simg is provided
* @returns {Boolean} true if sucessfully created canvas element
*/
function Simg(simg, image) {
  // Catch missing arguments
  Debug(simg);
  if (!simg) {
    throw new InvalidSimgParameters('Missing Simg arguments');
  }

  // Ensure that simg is an html element
  if (simg.nodeType == 1) {
  
    // Ensure that simg is actually an simg element  
    if (simg.nodeName.toLowerCase() != 'simg' || typeof simg.nodeName == 'undefined') {
      throw new InvalidSimgParameters('Invalid simg');
    }

  } else {
    throw new InvalidSimgParameters('Simg must be an HTML element');
  }
  
  var that = this;
  this.simg = simg;  // simg element
  this.image = image || null;  // actual image to display
  this.blankImageLoaded = false;  // image to display when blank is on

  // temporary until blankImage handling is created
  this.blankImage = new Image();
  this.blankImage.src = 'http://static.jquery.com/files/rocker/images/logo_jquery_215x53.gif';
  this.blankImage.onload = function() { that.blankImageLoaded = true; };

  // Create a canvas element
  this.canvas = document.createElement('canvas');
  this.ctx = this.canvas.getContext('2d');
  this.simg.parentNode.appendChild(this.canvas);
  Debug('Created canvas element: ' + this.canvas);

  // Load the image into the canvas element
  this.image = new Image();
  this.image.src = this.simg.getAttribute('src');
  this.image.onload = function() {
    Debug('Image loaded: ' + that.image.src);
    // resize the canvas
    that.canvas.width = that.image.width;
    that.canvas.height = that.image.height;
    
    // draw image
    that.ctx.drawImage(that.image, 0, 0);
  };

  // Copy the simg attributes to the canvas
  for (var i=0; i<this.simg.attributes.length; i++) {
    var attrib = this.simg.attributes[i];

    // Test to see if attribute is specified since IE6< will return all attributes including null/undefined
    if (attrib.specified) {
      // Handle IE6< by setting the attribute before writting to it with dot notation.
      // IE6< will not update the attribute unless it is set a certain way
      this.canvas.setAttribute(attrib.name, '');
      this.canvas[attrib.name] = attrib.value;
    }
  }
  Debug('Copied attributes to canvas');
/*
  // copy event handlers to the canvas
  for (var i=0; i<EVENTS.length; i++) {
    if (this.simg.getAttribute(EVENTS[i])) {
      this.canvas[EVENTS[i]] = this.simg[EVENTS[i]];
    }
  }
  Debug('Copied events to canvas');
*/
  // Remove the simg element
  //this.simg.parentNode.removeChild(this.simg);
  Info('Image created');
  return this;
};


  /**
  * Copies attributes from simg element to canvas element
  * @param {HTML Element} simg element
  * @param {HTML Element} canvas element
  * @returns {HTML Element} canvas element with copied attributes
  */
 /* var Simg.prototype._copyAttrib = function(simg, canvas) {
    for (var i=0; i<simg.attributes.length; i++) {
      var attrib = simg.attributes[i];

      // Test to see if attribute is specified since IE6< will return all attributes including null/undefined
      if (attrib.specified) {
        // Handle IE6< by setting the attribute before writting to it with dot notation.
        // IE6< will not update the attribute unless it is set a certain way
        canvas.setAttribute(attrib.name, '');
        canvas[attrib.name] = attrib.value;
      }
    }
  };*/


  /**
  * Blanks the image
  * @event
  */
  Simg.prototype.blankOn = function() {
    if (this.blankImageLoaded) {
    
      this.ctx.drawImage(this.blankImage, 0, 0);
      
    } else {
    
      // Fallback incase the image hasn't loaded yet
      this.ctx.fillStyle = "rgba(0,0,0,1)";
      this.ctx.fillRect(0,0,275,95);
    }
  }

  /**
  * Sets the image back to the default state after a blank
  * @event
  */
  Simg.prototype.blankOff = function() {
    this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
    this.ctx.drawImage(this.image, 0, 0);
  }

/**
* Manager for Simg's
* <ul><li>Finds all the simgs and creates Simg instances for each one</li>
* <li>Creates event handlers</li>
* <li>Acts as an event handler for all the Simg instances</li></ul>
* Created using Object Literal notation so that it can remain a singleton.
*/
var Manager = {
  Simgs: [],

  init: function() {
    // Find all the simg
    var simgs = document.getElementsByTagName('simg');
    Info('Found: ' + simgs.length + ' simg tags');
    
    // Create new Simg instances
    for (var i=0; i<simgs.length; i++){
      try {
        Debug('Creating simg instance: ' + i);
        var simg = new Simg(simgs[i]);
      } catch(e) {
        Warn(e.msg);
      }
      
      Manager.Simgs.push(simg);
      
    }
    // Bind to events
    window.onblur = function() { Manager.blankOn(); };
    window.onfocus = function() { Manager.blankOff(); };
    window.onkeydown = function(e) { 
      Debug('keydown');
      // detect control key
      if (e.keyCode == 17) {
        Manager.blankOn();
      }
    };
    window.onkeyup = function(e) {
      Debug('keyup');
      // detect release of control key
      if (e.keyCode == 17) {
        Manager.blankOff();
      }
    };
  },
  
  blankOn: function() {
    for (var i=0; i<Manager.Simgs.length; i++) {
      Manager.Simgs[i].blankOn();
    }
    Debug('Blank On');
  },
  
  blankOff: function() {
    for (var i=0; i<Manager.Simgs.length; i++) {
      Manager.Simgs[i].blankOff();
    }
  } 
}


/*
function Manager() {



}

  Manager.prototype.blankOn = function() {
  
  }
  
  Manager.prototype.blankOff = function() {
  
  }
  
*/
